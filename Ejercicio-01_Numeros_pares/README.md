# Enunciado del Problema

Elabora un programa que imprima los números pares del 0 al 100

# Análisis

* Se realiza un ciclo del 0 al 100
* Si el número es divisible entre 2 se imprime

# Entradas y Salidas

Salidas

Nombre | Descripción | Tipo
-------|-------------|------
p      | Número par  | Entero


