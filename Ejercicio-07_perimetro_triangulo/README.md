# Enunciado del Problema

Elabora programa que calcule el perímetro de un triángulo. El programa preguntará al usuario el tipo de triángulo (equilátero, isósceles o escaleno) y le pedirá las entradas necesarias para realizar el cálculo necesario.

# Análisis

* Se recibe la opción del tipo de triángulo.
* Dependiendo del tipo de triangulo se piden las entradas y se realizan los calculos correspondientes.
* Se imprime el valor del perímetro.

# Entradas y Salidas

Entradas

Nombre | Descripción     		                										| Tipo
-------|--------------------------------------------------------------------------------|--------
opcion | Opcion de acuerdo al tipo de triángulo 										|
lado   | La cantidad de variables de este tipo será diferente dependiendo el triángulo  | Flotante

Salidas

Nombre    | Descripción         | Tipo
----------|---------------------|--------
perimetro | Valor del perímetro | Flotante