def Equilatero(l):
    print("El perimetro del triangulo equilatero es: ",l*3)

def Isosceles(a,b):
    print("El perimetro del triangulo isosceles es: ",a*2+b)

def Escaleno(a,b,c):
    print("El perimetro del triangulo escaleno es: ",a+b+c)
    
def Perimetro(triangulo):
    if triangulo == 1:
        lado = float(input("Ingresa la medida de un lado del triangulo equilatero: "))
        Equilatero(lado)
    elif triangulo == 2:
        lado_a = float(input("Ingresa la medida de uno de los lados iguales del triangulo isosceles: "))
        lado_b = float(input("Ingresa la medida del lado diferente del triangulo isosceles: "))
        Isosceles(lado_a, lado_b)
    elif triangulo == 3:    
        lado_a = float(input("Ingresa la primer medida del triangulo escaleno: "))
        lado_b = float(input("Ingresa la segunda medida del triangulo escaleno: "))
        lado_c = float(input("Ingresa la tercer medida del triangulo escaleno: "))
        Escaleno(lado_a, lado_b, lado_c)
    else:
        print("Opcion incorrecta")
        
opcion = int(input("Ingresa el numero de opcion correspondiente al tipo de triangulo \n1.Equilatero\n2.Isosceles\n3.Escaleno\n"))
Perimetro(opcion)   

