# Enunciado del Problema

Elabora un programa calcule el perímetro de un triángulo equilátero. El programa pedirá al usuario las entradas necesarias.

# Análisis

* Se recibe la medida de un lado del triángulo.
* Se calcula el perímetro multiplicando la medida por tres.
* Se imprime el valor del perímetro.

# Entradas y Salidas

Entradas

Nombre | Descripción                     | Tipo
-------|---------------------------------|--------
lado   | Medida de un lado del triángulo | Flotante


Salidas

Nombre    | Descripción         | Tipo
----------|---------------------|--------
perimetro | Valor del perímetro | Flotante