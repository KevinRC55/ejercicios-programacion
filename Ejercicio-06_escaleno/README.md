# Enunciado del Problema

Elabora un programa calcule el perímetro de un triángulo escaleno. El programa pedirá al usuario las entradas necesarias.

# Análisis

* Se recibe la medida del primer lado del triángulo.
* Se recibe la medida del segundo lado del triángulo.
* Se recibe la medida del tercer lado del triángulo.
* Se calcula el perímetro sumando las tres medidas.
* Se imprime el valor del perímetro.

# Entradas y Salidas

Entradas

Nombre | Descripción     		               | Tipo
-------|---------------------------------------|--------
lado_a | Medida del primer lado del triángulo  | Flotante
lado_b | Medida del segundo lado del triángulo | Flotante
lado_c | Medida del tercer lado del triángulo  | Flotante

Salidas

Nombre    | Descripción         | Tipo
----------|---------------------|--------
perimetro | Valor del perímetro | Flotante
