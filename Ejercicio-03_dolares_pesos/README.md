# Enunciado del Problema

Elabora un programa que reciba como entrada un monto en dólares (USD) y devuelva la cantidad equivalente en pesos mexicanos (MXN).

# Análisis

* Se recibe una cantidad en dólares.
* Se multiplica la cantidad por el valor actual del peso frente al dólar.
* Se imprime el valor del monto en pesos.

# Entradas y Salidas

Entradas

Nombre | Descripción         | Tipo
-------|---------------------|--------
monto  | Cantidad en dólares | Flotante


Salidas

Nombre | Descripción       | Tipo
-------|-------------------|--------
monto  | Cantidad en pesos | Flotante