# Enunciado del Problema

Elabora un programa que reciba un número e imprima la tabla de multiplicar de ese número del 2 al 10.

# Análisis

* Se recibe un valor entero.
* En un ciclo se multiplica el valor recibido por el valor del contador del ciclo.
* Se imprimen los valores de las multiplicaciones.

# Entradas y Salidas

Entradas

Nombre | Descripción  | Tipo
-------|--------------|--------
numero | Valor entero | Entero 						

Salidas

Nombre    | Descripción                | Tipo
----------|----------------------------|--------
numero*i  | Valor de la multiplicación | Entero
