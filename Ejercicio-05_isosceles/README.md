# Enunciado del Problema

Elabora un programa calcule el perímetro de un triángulo  isósceles. El programa pedirá al usuario las entradas necesarias.

# Análisis

* Se recibe la medida de uno de los lados iguales del triángulo.
* Se recibe la medida del lado diferente del triángulo.
* Se calcula el perímetro multiplicando la medida a por 2 y sumando la medida b.
* Se imprime el valor del perímetro.

# Entradas y Salidas

Entradas

Nombre | Descripción                     					| Tipo
-------|----------------------------------------------------|--------
lado_a | Medida de uno de los lados iguales del triángulo   | Flotante
lado_b | Medida del lado diferente del triángulo 		    | Flotante

Salidas

Nombre    | Descripción         | Tipo
----------|---------------------|--------
perimetro | Valor del perímetro | Flotante
