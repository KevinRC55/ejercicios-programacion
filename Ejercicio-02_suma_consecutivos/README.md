# Enunciado del Problema

Elabora un programa que reciba un número entre 1 y 50 y devuelva la suma de los números consecutivos del 1 hasta ese número.

# Análisis

* Se recibe un número entre 1 y 50.
* En un ciclo de 1 al número recibido se suman los números.
* Se imprime el valor de la suma.

# Entradas y Salidas

Entradas

Nombre | Descripción      | Tipo
-------|------------------|--------
n      | Número de 1 a 50 | Entero


Salidas

Nombre | Descripción 		      | Tipo
-------|------------------------------|--------
suma   | Suma de números consecutivos | Entero

