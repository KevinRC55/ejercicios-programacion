def Suma_Consecutivos(n):
    if n > 50 or n < 1:
         print("Debe ingresar un numero entre 1 y 50")
    else:
        suma = 0
        for s in range(n):
            suma += s+1
        print("La suma de los numeros consecutivos del 1 al ",n," es: ",suma)
    
Suma_Consecutivos(50)

